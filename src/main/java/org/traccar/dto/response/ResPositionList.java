package org.traccar.dto.response;


import java.util.Date;

public class ResPositionList {

    private Date deviceTime;
    private String nameDevice;
    private int idDevice;
    private double latitude;
    private double longitude;
    private double altitude;
    private double speed;
    private String atributtes;
    private String status;
    private String model;
    private String category;
    private double course;

    public ResPositionList() {
    }

    public ResPositionList(Date deviceTime, String nameDevice, int idDevice, double latitude, double longitude, double altitude, double speed, String atributtes, String status, String model, String category, double course) {
        this.deviceTime = deviceTime;
        this.nameDevice = nameDevice;
        this.idDevice = idDevice;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.speed = speed;
        this.atributtes = atributtes;
        this.status = status;
        this.model = model;
        this.category = category;
        this.course = course;
    }

    public int getIdDevice() {
        return idDevice;
    }

    public void setIdDevice(int idDevice) {
        this.idDevice = idDevice;
    }

    public Date getDeviceTime() {
        return deviceTime;
    }

    public void setDeviceTime(Date deviceTime) {
        this.deviceTime = deviceTime;
    }

    public String getNameDevice() {
        return nameDevice;
    }

    public void setNameDevice(String nameDevice) {
        this.nameDevice = nameDevice;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getAtributtes() {
        return atributtes;
    }

    public void setAtributtes(String atributtes) {
        this.atributtes = atributtes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getCourse() {
        return course;
    }

    public void setCourse(double course) {
        this.course = course;
    }
}
