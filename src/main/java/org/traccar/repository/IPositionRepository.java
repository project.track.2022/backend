package org.traccar.repository;


import org.springframework.stereotype.Repository;
import org.traccar.dto.response.ResPositionList;
import org.traccar.model.Position;
import org.traccar.storage.DatabaseModule;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class IPositionRepository {

    public List<ResPositionList> fetchData(long userid) throws SQLException {
        String SQL_QUERY = "SELECT dev.id,dev.name,dev.status,dev.model,dev.category,pos.latitude," +
                "pos.altitude,pos.longitude,pos.speed,pos.course,pos.attributes,pos.devicetime " +
                "FROM traccar.tc_devices as dev " +
                "INNER JOIN traccar.tc_user_device user on user.deviceid = dev.id " +
                "INNER JOIN traccar.tc_positions pos on pos.id = dev.positionid " +
                "WHERE user.userid = '" + userid + "' " +
                "ORDER BY pos.devicetime DESC";
        List<ResPositionList> listPositions = null;
        try (Connection con = DatabaseModule.getConnection();
             PreparedStatement pst = con.prepareStatement(SQL_QUERY);
             ResultSet rs = pst.executeQuery();) {
            listPositions = new ArrayList<>();
            ResPositionList position;
            while (rs.next()) {
                position = new ResPositionList();
                position.setLatitude(rs.getDouble("latitude"));
                position.setLongitude(rs.getDouble("longitude"));
                position.setIdDevice(rs.getInt("id"));
                position.setNameDevice(rs.getString("name"));
                position.setAltitude(rs.getDouble("altitude"));
                position.setModel(rs.getString("model"));
                position.setCategory(rs.getString("category"));
                position.setSpeed(rs.getDouble("speed"));
                position.setCourse(rs.getDouble("course"));
                position.setAtributtes(rs.getString("attributes"));
                position.setStatus(rs.getString("status"));
                position.setDeviceTime(rs.getDate("devicetime"));

                listPositions.add(position);
            }
        }
        return listPositions;
    }

}
